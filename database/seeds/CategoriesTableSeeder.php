<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'id' => 1,
                'name' => 'MeatShare',
                'description' => 'MeatHub MeatShare category. ',
                'created_at' => Carbon::today(),
                'updated_at' => Carbon::today(),
            ],
            [
                'id' => 2,
                'name' => 'Retail',
                'description' => 'MeatHub Retail category. ',
                'created_at' => Carbon::today(),
                'updated_at' => Carbon::today(),
            ],
        ];


        foreach ($categories as $category) {
            DB::table('categories')->insert([
                'id' => $category['id'],
                'name' => $category['name'],
                'description' => $category['description'],
            ]);
        }
    }
}

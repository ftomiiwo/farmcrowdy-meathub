<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', 'API\RegisterController@returnUsers');

Route::post('register', 'API\RegisterController@authenticate');
Route::post('login', 'API\RegisterController@authenticate');
Route::post('otp/resend', 'API\RegisterController@authenticate');
Route::post('otp/verify', 'API\RegisterController@verifyOTP');

Route::get('mereuser', 'API\RegisterController@mereUser');
Route::middleware('auth:api')->group(function () {
    Route::group(['prefix' => 'onboard'], function () {
        Route::post('/setup-account', 'API\OnboardController@accountSetup');
    });
});

Route::group(['prefix'=> 'password'], function (){
    Route::post('/request', 'API\RegisterController@requestPasswordChange');//request for a password change
    Route::post('/update', 'API\RegisterController@updatePassword');//Update default user password.
    Route::post('/change', 'API\RegisterController@changePassword');//take this down soon
});

Route::middleware('auth:api')->group(function () {
    Route::resource('products', 'API\ProductController');
    Route::get('/user', 'API\RegisterController@returnUsers');
});

Route::group(['prefix' => 'category'], function () {
   Route::post('create', 'API\CategoryController@create');
   Route::get('all', 'API\CategoryController@index');
   Route::get('{category_id}', 'API\CategoryController@show');
   Route::patch('edit/{category_id}', 'API\CategoryController@update');
   Route::delete('delete/{category_id}', 'API\CategoryController@destroy');
});

Route::group(['prefix' => 'product'], function () {
    Route::post('create', 'API\ProductController@store');
    Route::get('all', 'API\ProductController@index');
    Route::get('{product_id}', 'API\ProductController@show');
    Route::patch('edit/{product_id}', 'API\ProductController@update');
    Route::delete('delete/{product_id}', 'API\ProductController@destroy');
});



## About Farmcrowdy MeatHub

 

The MeatHub Project brings technology to meat sharing

## Contributing

 - Pull from the master branch
 - Create a branch of your own
   - Branch naming should follow the following convention. 
   If you're working on a feature, name the branch feature-{feature-description}
   If you're working on a fix, name the branch fix-{issue-description}
   If you're working on a milestone, name the branch milestone-{milestone-description}
   
   
## Command to run
- composer install
- npm install
- php artisan key:generate
- php artisan migrate
- php artisan passport:install
- php artisan db:seed
   
## Code of Conduct

When you branch out of an existing branch, alway make sure you pull from the branch you want to merge into before pushing to the remote repository

when you're done working on your fix (or feature, or milestone), always make sure to create a pull request (PR), attach who should review. 

Send an email to follow up incase the review is pending for long. 

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Developers via [developer@farmcrowdy.com](mailto:developer@farmcrowdy.com).
 All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

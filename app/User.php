<?php

namespace App;

use App\Models\UserLocation;
use App\Models\UserRole;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone_number', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param string $id_or_phone
     * @return mixed
     */
    public static function getUser(string $id_or_phone){
        return self::where('id', $id_or_phone)->orWhere('phone_number', $id_or_phone)->first();
    }

    /**
     * @param $columnName
     * @param $value
     * @return mixed
     */

    public static function checkIfExists($columnName, $value){
        return self::where($columnName, $value)->first();
    }

    public static function createNewUserWithPhone($phone_number)
    {
        $newUser = new User();

        $newUser->phone_number = $phone_number;
        $newUser->password = self::protectPassword($phone_number);
        $newUser->save();

        $userRole = new UserRole();
        $userRole->user_id = $newUser->id;
        $userRole->role_id = 1;
        $userRole->save();

        return $newUser;
    }

    /**
     * create a new user
     * @param $name
     * @param $phone
     * @param $email
     * @param $password
     * @return User
     */
    public static function createNewUser($name, $phone, $email, $password){
        $role = 1;//set a default user role as Rider...
        $newUser = new User();
//        $newUser->role_id = $role;
        $newUser->name = $name;
        $newUser->phone_number = $phone;
        $newUser->email = $email;
        $newUser->password = self::protectPassword($password);
        $newUser->is_default_pass_changed = 'Yes';
        $newUser->save();

        $id = $newUser->id;
        $newUser->id = $id;

        $userRole = new UserRole();
        $userRole->user_id = $newUser->id;
        $userRole->role_id = 1;
        $userRole->save();

        return $newUser;
    }


    /**
     * update set or related fields
     * @param $usrID
     * @param array $fields
     * @return mixed
     */
    public static function updateRecord($usrID, array $fields){
        $User = User::find($usrID);

        if(count($fields) > 0) {
            foreach ($fields as $key => $value) {
                $column = $key;
                $val = $value;

                if($column === 'password') {
                    $User->$column = self::protectPassword($val);
                } else {
                    $User->$column = $val;
                }
            }
            $User->save();
        }
        return $User;
    }

    /**
     * @param $password
     * @return string
     */
    public static function protectPassword($password){
        return Hash::make($password);
    }

    public function user_role()
    {
        return $this->hasOne(UserRole::class, 'role_id', 'id');
    }
}

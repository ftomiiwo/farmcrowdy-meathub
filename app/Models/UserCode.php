<?php

namespace App\Models;

use App\Helper;
use App\Helpers\SMS;
use App\Http\Controllers\OnboardController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

use App\User;

class UserCode extends Model
{
    public $fillable = ['user_id', 'otp', 'is_used'];

    public function driver(){
        return $this->belongsTo(User::class, 'driver_id');
    }

    /**
     * Find a User OTP Code data
     * @param int $otp
     * @return mixed
     */
    public static function findByCode(int $otp){
        return self::where('otp', $otp)->first();
    }

    /**
     * fetch the User code details.
     * @param int $userID
     * @return mixed
     */
    public static function getUserCode(int $userID){
        return UserCode::where('user_id', $userID)->first();
    }

    /**
     * find a user OTP code
     * @param int $userID
     * @param string $phone_number
     * @param null $type
     * @return int|string
     */
    public static function createNewOTP(int $userID, string $phone_number){
        //create the user's otp code record here
        $otp = Helper::getMobileOTP();
        $User = self::getUserCode($userID);

        if(!$User){
            $newOTP = new self();
            $newOTP->user_id = $userID;
            $newOTP->otp = $otp;
            $newOTP->save();

        }else{
            $User->otp = $otp;
            $User->is_used = 0;
            $User->save();


        }

        SMS::sendOTP($phone_number, $otp);

        return $otp;
    }

    /**
     * validates the codes of a user
     * @param int $otp
     * @param string $requestPhone
     * @return \Illuminate\Http\JsonResponse
     */
    public static function validateOTP(int $otp, string $requestPhone){
        $UserCode = self::findByCode($otp);
        if(!$UserCode) return jsonResponse(false, 'This OTP is invalid!');
        if($UserCode->is_used) return jsonResponse(false, 'This OTP has already been used.');

        $User = User::find($UserCode->user_id);
        if(!$User) return Helper::jsonResponse(false, 'User not found!');
        if($User->phone_number !== $requestPhone) return Helper::jsonResponse(false, 'Phone number mismatched.');

        $updated_at = Carbon::make($UserCode->updated_at);
        if(Helper::validateOTP($updated_at)) return Helper::jsonResponse(false, 'OTP already expired');
    }

    /**
     * update the user otp record
     * @param int $userID
     */
    public static function updateCodeRec (int $userID){
        $User = self::getDriverCode($userID);
        if($User){
            $User->is_used = 1;
            $User->save();
        }
    }

    /**
     * fetch the driver code details.
     * @param int $driverID
     * @return mixed
     */
    public static function getDriverCode(int $userID){
        return self::where('user_id', $userID)->first();
    }

}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper;
use Illuminate\Support\Facades\Validator;

use App\Models\Category;

class CategoryController extends Controller
{

    public function index() {
        $categories = Category::all();
        return Helper::jsonResponse(true, 'Please check your email for the verification code.', 200, ['categories' => $categories]);

    }

    public function show($id)
    {
        $category = Category::find($id);

        if (is_null($category)) {
            return Helper::jsonResponse(false, 'category not found', 400);
        }

        return Helper::jsonResponse(true, 'category retrieved successfully.', 200, ['category' => $category]);

    }

    public function create(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $category = new Category();
        $category->name = $input['name'];
        $category->description = $input['description'];
        $category->save();

        if($category) {
            return Helper::jsonResponse(true, "Category created successfully", 200, ['category' => $category]);
        }

    }

    public function update(Request $request, int $category_id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $category = Category::find($category_id);
        $category->name = $input['name'];
        $category->description = $input['description'];
        $category->save();

        if ($category) return Helper::jsonResponse(true, 'Category updated successfully.', 200, ['category' => $category]);
    }


    public function destroy(int $category_id)
    {
        $category = Category::find($category_id);
        $category->delete();

        if ($category) return Helper::jsonResponse(true, 'Category record deleted successfully.', 200);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use Illuminate\Http\Request;
use App\User;
use App\Models\UserCode;
use App\Models\UserLocation;
use App\Helper;
use Illuminate\Support\Str;

class OnboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function accountSetup(Request $request)
    {
        $email_address = $request->email;
        $fullname = $request->name;
        $password = $request->password;
        $location = $request->location_id;
        $phone_number = $request->phone_number;
        $isEmpty = ['email address'=>$email_address, 'password'=>$password, 'location' => $location];
        if(Helper::checkEmptyFields($isEmpty)){return Helper::jsonResponse(false, Helper::checkEmptyFields($isEmpty));}
        if(!filter_var($email_address, FILTER_VALIDATE_EMAIL))return Helper::jsonResponse(false, 'Invalid email address.');

        $Email = User::checkIfExists('email', $email_address);
        $userInSession = User::where('phone_number', $phone_number)->first();
        $User = null;
        if (!$Email) {
            $User = User::updateRecord(
                $userInSession->id,
                [
                    'name' => $fullname,
                    'email' => $email_address,
                    'password'=> $password,
                    'is_default_pass_changed'=> 'Yes'
                ]
            );

            $locationObject = new UserLocation();
            $locationObject->user_id = $userInSession->id;
            $locationObject->location_id = $request->location;
            $locationObject->save();

            $emailToken = Str::random(15);
            //send a verification email to the enrolled user.
            $recipient_name = $fullname;
            $d = ['recipient_name'=>$recipient_name];
            $configData = [
                'sender_email'=> getenv('APP_EMAIL'),
                'sender_name'=> getenv('APP_NAME'),
                'recipient_email'=> $email_address,
                'recipient_name'=> $recipient_name,
                'subject'=> 'Welcome'
            ];

            EmailController::dispatchMail($configData, 'emails.welcome', $d);
        }

        return Helper::jsonResponse(true, 'Please check your email for the verification code.', 200, 'Welcome');
    }
}
